package pl.sda.Generyczne;

import java.util.Arrays;

/**
 * Stwórz klasę generyczną Para, która przyjmuje dwa obiekty - obiekt prawy i lewy.
 * Niech klasa będzie generyczna. Typy obiektów (lewego i prawego) klasy mają być podane
 * przy tworzeniu obiektu (generyczne). Stwórz kilka obiektów tego typu a następnie dodaj je do tablicy.
 *
 *
 * Stwórz metodę generyczną 'znajdźNiepuste(Para[]):Para[]' która iteruje tablicę obiektów typu 'Para'
 * i znajduje tylko niepuste pary (obiekt prawy i lewy mają być niepuste (!=null).
 */

public class Para<T> {
    T prawy;
    T lewy;

    @Override
    public String toString() {
        return "Para{" +
                "prawy=" + prawy +
                ", lewy=" + lewy +
                '}';
    }

    public static Para[] znajdzNiepuste(Para[] paras){
        int rozmiar=0;
        for (Para parka:paras) {
            if (parka.czyNepusta()){
                rozmiar++;
            }
        }
        int indeksWsat=0;
        Para[] niepuste= new Para[rozmiar];

        for (Para parka:paras){
            if(parka.czyNepusta()){
                niepuste[indeksWsat++]= parka;
            }
        }
        return niepuste;
    }

    public T getPrawy() {
        return prawy;
    }

    public void setPrawy(T prawy) {
        this.prawy = prawy;
    }

    public T getLewy() {
        return lewy;
    }

    public void setLewy(T lewy) {
        this.lewy = lewy;
    }

    private boolean czyNepusta() {
     if (getPrawy()!=null&&getLewy()!=null){
         return true;
     }return false;
}

    public Para(T prawy, T lewy) {
        this.prawy = prawy;
        this.lewy = lewy;
    }

    public static void main(String[] args) {
        zad1 zad1 = new zad1("nazwa");
        zad1 zad2 = new zad1("nazwa1");
        zad1 zad3 = new zad1("");

        Para<zad1> para = new Para(zad1,zad2);
        Para<zad1> para1 = new Para(zad1,zad2);
        Para<zad1> para2 = new Para(zad1,zad2);
        Para<zad1> para3 = new Para(zad1,zad2);
        Para<zad1> para4 = new Para(zad1,zad3);

        Para[] pary = new Para[]{para,para1,para2,para3};


        ;
        System.out.println(Arrays.toString(znajdzNiepuste(pary)));



    }
}
