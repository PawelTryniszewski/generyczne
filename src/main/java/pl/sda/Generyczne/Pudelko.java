package pl.sda.Generyczne;

/**
 * Stwórz klasę generyczną Pudełko. Pudełko przechowuje dowolny obiekt
 * generyczny. Dodaj pole typu generycznego 'T'. Stwórz konstruktor który
 * przyjmuje jako parametr obiekt generyczny (przypisz jego wartość do pola).
 * Dodaj getter oraz setter do klasy.
 *
 *
 * dodaj do klasy metodę "czyPudełkoJestPuste():boolean" która zwraca informację
 * true - jeśli pudełko jest puste (czyli obiekt generyczny jest ==null) oraz false
 * gdy nie jest puste (obiekt [pole] generyczne jest !=null).
 *
 */
public class Pudelko<T> {

    private T dowolnyObiekt;


    public T getDowolnyObiekt() {
        return dowolnyObiekt;
    }

    public void setDowolnyObiekt(T dowolnyObiekt) {
        this.dowolnyObiekt = dowolnyObiekt;
    }

    public Pudelko(T dowolnyObiekt) {
        this.dowolnyObiekt = dowolnyObiekt;

    }
    public  Boolean czyPudloPuste(T dowolnyObiekt){
        if (dowolnyObiekt==null){

            return true;
        }else
            return false;

    }
}
