package pl.sda.Generyczne;

/**
 * Stwórz statyczną metodę generyczną, która 10 razy wypisuje
 * na ekran metodę toString podanego parametru.
 */
public class zad1 {
    String nazwa;

    public zad1(String nazwa) {
        this.nazwa = nazwa;
    }

    public static void main(String[] args) {
        String nazwa;
        nazwa= "Pawel";
        wypisz10razy(nazwa);

    }

    @Override
    public String toString() {
        return "zad1{" +
                "nazwa='" + nazwa + '\'' +
                '}';
    }

    public static <T> void wypisz10razy(T... nazwa) {

        for (T obiekt : nazwa) {
            for (int i = 0; i < 10; i++) {
                System.out.println(nazwa);
            }

        }

    }

}
