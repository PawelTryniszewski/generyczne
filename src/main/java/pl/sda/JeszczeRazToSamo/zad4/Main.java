package pl.sda.JeszczeRazToSamo.zad4;

/**
 * Stwórz metodę generyczną kóra dla dowolnych dwóch parametrów (dziedziczących po
 * klasie Number (wykorzystaj metodę generyczną ze słówkiem extends) ) zwraca ich sumę
 * (jako double). Metoda ma przyjmować dwie liczby typu T i zwracać wynik typu double.
 */
public class Main {

    static  <T extends Number> double suma(T parametr1, T parametr2) {

        return parametr1.doubleValue()+parametr2.doubleValue();

    }

    public static void main(String[] args) {
        System.out.println(suma(43,434));
    }

}
