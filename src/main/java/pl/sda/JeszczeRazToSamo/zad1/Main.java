package pl.sda.JeszczeRazToSamo.zad1;

import java.sql.SQLOutput;

/**
 * 1. Stwórz statyczną metodę generyczną, która 10 razy wypisuje na ekran
 * metodę toString podanego parametru - obiektu podanego jako argument metody.
 */
public class Main<T> {
    private T napis;

    public Main(T napis) {
        this.napis = napis;
    }


    public static <T> void wypisz(T napis) {
        for (int i = 0; i < 9; i++) {
            System.out.println(napis);
        }
    }

    public static void main(String[] args) {
        wypisz("Kutas");
    }

}
