package pl.sda.JeszczeRazToSamo.zad8;

public class Samochod {
    String marka;
    String model;
    String kolor;

    public Samochod(String marka, String model, String kolor) {
        this.marka = marka;
        this.model = model;
        this.kolor = kolor;
    }

    @Override
    public String toString() {
        return marka +" " +model +" "+ kolor ;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }
}
