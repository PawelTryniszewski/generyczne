package pl.sda.JeszczeRazToSamo.zad8;

public class Porshe extends Samochod {
    boolean czyElektryczny;

    @Override
    public String toString() {
        return marka + " " +" " + model +" " + kolor + " Elektryczny: " +czyElektryczny;
    }

    public Porshe(String marka, String model, String kolor, boolean czyElektryczny) {
        super(marka,model,kolor);
        this.marka = marka;
        this.model = model;
        this.kolor = kolor;
        this.czyElektryczny=czyElektryczny;
    }
}
