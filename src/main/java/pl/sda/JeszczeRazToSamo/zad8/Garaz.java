package pl.sda.JeszczeRazToSamo.zad8;

public class Garaz<T> {
    private T miejsce1;
    private Object ObaZajete;

    @Override
    public String toString() {
        return "Garaz{" +
                "miejsce1=" + miejsce1 +
                ", miejsce2=" + miejsce2 +
                '}';
    }

    private T miejsce2;

    public void zaparkujAuto(Samochod samochod){
        if (miejsce1==null){
            setMiejsce1((T) samochod);
        }else if (miejsce2==null){
            setMiejsce2((T) samochod);
        }else if (miejsce1!=null&&miejsce2!=null){
            throw new ObaZajete("Oba miejsca są zajęte");

        }
    }
    public void wyprowadzAuto(Samochod samochod){
        if (miejsce1==samochod){
            setMiejsce1(null);
        }else if (miejsce2==samochod){
            setMiejsce2(null);
        }
    }

    public Garaz(T miejsce1, T miejsce2) {
        this.miejsce1 = miejsce1;
        this.miejsce2 = miejsce2;
    }

    public T getMiejsce1() {
        return miejsce1;
    }

    public void setMiejsce1(T miejsce1) {
        this.miejsce1 = miejsce1;
    }

    public T getMiejsce2() {
        return miejsce2;
    }

    public void setMiejsce2(T miejsce2) {
        this.miejsce2 = miejsce2;
    }
}
