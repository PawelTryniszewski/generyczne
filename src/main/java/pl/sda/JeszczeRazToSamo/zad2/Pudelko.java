package pl.sda.JeszczeRazToSamo.zad2;

/**
 * Stwórz klasę generyczną Pudełko. Pudełko przechowuje dowolny obiekt
 * generyczny. Dodaj pole typu generycznego 'T'. Stwórz konstruktor który
 * przyjmuje jako parametr obiekt generyczny (przypisz jego wartość do pola). Dodaj getter oraz setter do klasy.
 *
 *
 * dodaj do klasy metodę "czyPudełkoJestPuste():boolean" która zwraca informację true -
 * jeśli pudełko jest puste (czyli obiekt generyczny jest ==null) oraz false gdy nie jest puste
 * (obiekt [pole] generyczne jest !=null).
 */
public class Pudelko<T> {
    private T dowolyObiekt;

    public static <T> boolean czyPudelkoPuste(T dowolnyobiekt){
        if (dowolnyobiekt!=null){
            return false;
        }else
            return true;
    }

    public Pudelko(T dowolyObiekt) {
        this.dowolyObiekt = dowolyObiekt;
    }

    public T getDowolyObiekt() {
        return dowolyObiekt;
    }

    public void setDowolyObiekt(T dowolyObiekt) {
        this.dowolyObiekt = dowolyObiekt;
    }


    public static void main(String[] args) {
        Pudelko pudelko =new Pudelko(3);
        System.out.println(pudelko.getDowolyObiekt());
        pudelko.setDowolyObiekt("banan");
        System.out.println(pudelko.getDowolyObiekt());
        pudelko.setDowolyObiekt(true);
        System.out.println(pudelko.getDowolyObiekt());
        pudelko.setDowolyObiekt(null);
        System.out.println(czyPudelkoPuste(pudelko.getDowolyObiekt()));



    }
}
