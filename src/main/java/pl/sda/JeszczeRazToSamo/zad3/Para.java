package pl.sda.JeszczeRazToSamo.zad3;

import java.util.ArrayList;

/**
 * Stwórz klasę generyczną Para, która przyjmuje dwa obiekty - obiekt prawy i lewy.
 * Niech klasa będzie generyczna. Typy obiektów (lewego i prawego) klasy mają być
 * podane przy tworzeniu obiektu (generyczne). Stwórz kilka obiektów tego typu a
 * następnie dodaj je do tablicy.
 */
public class Para<T> extends ArrayList<Para<Kursant>> {
    private T prawy;
    private T lewy;


    Para(ArrayList<Para> lista) {
    }


    Para(T prawy, T lewy) {
        this.prawy = prawy;
        this.lewy = lewy;
    }

    void setPrawy(T prawy) {
        this.prawy = prawy;
    }

    void setLewy(T lewy) {
        this.lewy = lewy;
    }

    private T getPrawy() {
        return prawy;
    }

    private T getLewy() {
        return lewy;
    }

    @Override
    public String toString() {
        return "Para " + prawy + lewy;
    }

    ArrayList<Para<Kursant>> niepelnePary(ArrayList<Para<Kursant>> listaPar) {
        ArrayList<Para<Kursant>> nowaLista = new ArrayList<>();
        for (Para<Kursant> element : listaPar) {
            if (element.getPrawy() != null && element.getLewy() != null) {
                nowaLista.add(element);
            }
        }
        return nowaLista;
    }

}
