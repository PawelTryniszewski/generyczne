package pl.sda.JeszczeRazToSamo.zad3;
/*
  Stwórz metodę generyczną 'znajdźNiepuste(Para[]):Para[]' która iteruje tablicę obiektów typu
  'Para' i znajduje tylko niepuste pary (obiekt prawy i lewy mają być niepuste (!=null).
 */

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Kursant pawel = new Kursant("Pawel", "Tryniszewski", 27, "Kursant");
        Kursant dawid = new Kursant("Dawid", "Rojda", 28, "Kursant");
        Kursant mateusz = new Kursant("Mateusz", "Kaminski", 28, "Kursant");
        Kursant kuba = new Kursant("Jakub", "Kapibara", 11, "Kursant");
        Kursant klaudia = new Kursant("Klaudia", "Nowak", 25, "Kursant");
        Kursant dominik = new Kursant("Dominik", "Tokarczyk", 24, "Kursant");
        Kursant jaromir = new Kursant("Jaro", "Putrycz", 600, "Kursant");
        Kursant michal = new Kursant("Michal", "Miloch", 30, "Marynarz");
        Kursant kasandra = new Kursant("Tomek", "Kowalski", 29, "Kursant");
        Kursant preclaw = new Kursant("Pawel", "Reclaw", 25, "Naczelny Trener");
        Kursant dubi = new Kursant("Piter", "Dubidu", 31, "Major");
        Kursant kasia = new Kursant("Kasia ", "Wielgocostam", 34, "Opiekun");

        Para<Kursant> naczelna = new Para<Kursant>(dubi, preclaw);
        Para<Kursant> pierwsza = new Para<Kursant>(pawel, dawid);
        Para<Kursant> druga = new Para<Kursant>(mateusz, kuba);
        Para<Kursant> trzecia = new Para<Kursant>(michal, kasandra);
        Para<Kursant> geje = new Para<Kursant>(klaudia, jaromir);
        Para<Kursant> dziwnaPara = new Para<Kursant>(kasia, dominik);

        Para[] paryKursantow = new Para[]{naczelna, pierwsza, druga, trzecia, geje, dziwnaPara};
        ArrayList<Para<Kursant>> listaPar = new ArrayList<>();
        listaPar.add(naczelna);
        listaPar.add(pierwsza);
        listaPar.add(druga);
        listaPar.add(trzecia);
        listaPar.add(geje);
        listaPar.add(dziwnaPara);
        Para para = new Para(listaPar);
        geje.setLewy(null);
        naczelna.setLewy(null);
        trzecia.setPrawy(null);
        dziwnaPara.setPrawy(null);



        System.out.println(para.niepelnePary(listaPar));


    }

}
