package pl.sda.JeszczeRazToSamo.zad3;

public class Kursant<T> {
    private T imie;
    private T nazwisko;
    private T wiek;
    private T stanowisko;

    @Override
    public String toString() {
        return imie + " " + nazwisko +
                " lat " + wiek + " " + stanowisko + " ";
    }

    public T getImie() {
        return imie;
    }

    public void setImie(T imie) {
        this.imie = imie;
    }

    public T getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(T nazwisko) {
        this.nazwisko = nazwisko;
    }

    public T getWiek() {
        return wiek;
    }

    public void setWiek(T wiek) {
        this.wiek = wiek;
    }

    public T getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(T stanowisko) {
        this.stanowisko = stanowisko;
    }

    public Kursant(T imie, T nazwisko, T wiek, T stanowisko) {

        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.stanowisko = stanowisko;
    }
}
