package pl.sda.JeszczeRazToSamo.zad5;

/**
 * Stwórz statyczną metodę generyczną 'abs:T' która dla dowolnej liczby zwraca jej :
 *     - wartość bezwzględną.
 *     - wartość przeciwną,
 *     - wartość odwrotną
 *
 * DO REALIZACJI ZADANIA NIE UŻYWAJ METOD KLASY 'Math'.
 */
public class Main {

    public static  <T extends Number> void abs(T liczba){
        if (liczba.intValue()>0){
            System.out.println("Wartosc bezwzględna: |"+liczba+"|");
        }else System.out.println("Wartosc bezwzględna: |"+liczba.doubleValue()*(-1)+"|");

        System.out.println("Liczba przeciwna: "+liczba.doubleValue()*(-1));

        System.out.println("Liczba odwrotna: "+1/liczba.doubleValue());
    }

    public static void main(String[] args) {
        abs(-41);
    }

}
