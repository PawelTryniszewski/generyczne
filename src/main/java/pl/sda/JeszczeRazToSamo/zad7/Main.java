package pl.sda.JeszczeRazToSamo.zad7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Napisz statyczną metodę generyczną, która jako argument przyjmuje (varargs)
 * różne elementy, a następnie (wewnątrz metody) umieszcza elementy obu list w jednej
 * liście i zwraca ją jako wynik. Metoda powinna być generyczna i przyjmować tylko listy tego
 * samego typu (parametr varargs to : ArrayList<T>... listy)
 */

public class Main {




    public static <T> T varargsListy(ArrayList<T>... listy) {

        ArrayList<T> nowaLista = new ArrayList<>();
        for (Object obiekty : listy) {
            nowaLista.add((T) obiekty);

        }
        nowaLista.toArray();
        nowaLista.spliterator();



        return (T) nowaLista;

    }



    public static void main(String[] args) {
        Kot kot = new Kot("kot");
        Kot kot1 = new Kot("miau");
        Kot kot2 = new Kot("miau");
        Kot kot3 = new Kot("miau");
        Kot kot4 = new Kot("miau");
        Kot kot5 = new Kot("miau");
        Kot kot6 = new Kot("miau");
        Kot kot7 = new Kot("miau");

        ArrayList<Kot> lista1 = new ArrayList<>();
        ArrayList<Kot> lista2 = new ArrayList<>();

        lista1.add(kot);
        lista1.add(kot1);
        lista1.add(kot2);
        lista1.add(kot3);

        lista2.add(kot4);
        lista2.add(kot5);
        lista2.add(kot6);
        lista2.add(kot7);

        System.out.println(varargsListy(lista1, lista2));

    }
}
