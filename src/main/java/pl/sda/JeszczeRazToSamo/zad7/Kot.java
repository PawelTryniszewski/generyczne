package pl.sda.JeszczeRazToSamo.zad7;

import java.util.ArrayList;

public class Kot<T>  {
    String imie;

    public Kot(String imie) {
        this.imie = imie;
    }

    @Override
    public String toString() {
        return "Kot " +imie;
    }
}
