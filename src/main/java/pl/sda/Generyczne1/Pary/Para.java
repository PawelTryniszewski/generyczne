package pl.sda.Generyczne1.Pary;

public class Para <T>{
    private T lewy;
    private T prawy;

    public Para(T lewy, T prawy){
        this.lewy=lewy;
        this.prawy=prawy;
    }

    public T wezLewy(){
        return lewy;
    }
    public T wezPrawy(){
        return prawy;
    }

    @Override
    public String toString() {
        return "Para{" +
                "lewy=" + lewy +
                ", prawy=" + prawy +
                '}';
    }
}
