package pl.sda.Generyczne1.Pary;


public class Main {
    public static void main(String[] args) {

        Czlowiek monika = new Czlowiek("Monika","Kowalska",Plec.KOBIETA);
        Czlowiek adam = new Czlowiek("Adam","Nowak",Plec.MEZCZYZNA);
        Para <Czlowiek> para= new Para<Czlowiek>(monika,adam);
        System.out.println(para);
        para.wezLewy().setNazwisko("NoweNazwisko");
        System.out.println(para.wezLewy().getNazwisko());

    }
}
