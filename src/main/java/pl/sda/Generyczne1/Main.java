package pl.sda.Generyczne1;

public class Main {
    public static void main(String[] args) {
        int [] intTab = new int[]{1,2,3,2,4,3,5};
        Integer[] integersTab = new Integer[]{1,2,3,2,4,3,5};

        Character[] czTab = new Character[]{'a','c','E','!','`','g'};
        String a,b,c,d;
        a="ala";b="basia";c="celina";d="danuta";
        String[] strTab= new String[]{a,b,c,d};

        wypisz(integersTab);
        System.out.println();
        wypisz(czTab);
        System.out.println();
        wypisz(strTab);


    }
    public static <T> void wypisz(T[] tablica){
        for (T element:tablica) {
            System.out.println(element);
            
        }
    }
}
